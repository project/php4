<?php

/**
 * @file
 *   Include file
 *
 * @version
 *
 * @developers
 *   Rafal Wieczorek <kenorb@gmail.com>
 */


## Defines only available in PHP 5, created for PHP4
if(!defined('PHP_URL_SCHEME')) define('PHP_URL_SCHEME', 1);
if(!defined('PHP_URL_HOST')) define('PHP_URL_HOST', 2);
if(!defined('PHP_URL_PORT')) define('PHP_URL_PORT', 3);
if(!defined('PHP_URL_USER')) define('PHP_URL_USER', 4);
if(!defined('PHP_URL_PASS')) define('PHP_URL_PASS', 5);
if(!defined('PHP_URL_PATH')) define('PHP_URL_PATH', 6);
if(!defined('PHP_URL_QUERY')) define('PHP_URL_QUERY', 7);                        
if(!defined('PHP_URL_FRAGMENT')) define('PHP_URL_FRAGMENT', 8);    
    
/**
 * Replace parse_url()
 *
 * @category    PHP
 * @link        http://php.net/parse_url
 * @require     PHP 4.x ???
 */
function parse_url_compat($url, $component=NULL){
    if(!$component) return parse_url($url);
    
    ## PHP 5
    if(phpversion() >= 5)
        return parse_url($url, $component);

    ## PHP 4
    $bits = parse_url($url);
    
    switch($component){
        case PHP_URL_SCHEME: return $bits['scheme'];
        case PHP_URL_HOST: return $bits['host'];
        case PHP_URL_PORT: return $bits['port'];
        case PHP_URL_USER: return $bits['user'];
        case PHP_URL_PASS: return $bits['pass'];
        case PHP_URL_PATH: return $bits['path'];
        case PHP_URL_QUERY: return $bits['query'];
        case PHP_URL_FRAGMENT: return $bits['fragment'];
    }
    
}

